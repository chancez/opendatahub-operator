AI Library
=========

The role will deploy the AI Library models via ```SeldonDeployments```.

Dependencies
------------

Seldon must be installed and running in the same namespace the models are being deployed.

The installation can be done manually or via the Open Data Hub operator.

Role Variables
--------------

None

Example Variable Spec
---------------------

```
ai-library:
  odh_deploy: true
```

License
-------

GNU GPLv3

Author Information
------------------

croberts@redhat.com
