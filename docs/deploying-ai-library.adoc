// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>

// Base the file name and the ID on the module title. For example:
// * file name: doing-procedure-a.adoc
// * ID: [id="doing-procedure-a"]
// * Title: = Doing procedure A

// The ID is used as an anchor for linking to the module. Avoid changing it after the module has been published to ensure existing links are not broken.
[id="deploying-ai-library"]
// The `context` attribute enables module reuse. Every module's ID includes {context}, which ensures that the module has a unique ID even if it is reused multiple times in a guide.
= Deploying AI Library Setup
// Start the title of a procedure module with a verb, such as Creating or Create. See also _Wording of headings_ in _The IBM Style Guide_.

This procedure describes how to enable (and disable) AI Library for the Open Data Hub

.Prerequisites

* A terminal shell with the OpenShift client command `oc` available.
* A copy of the files in this repository available in your terminal shell.
* The ability to run the operator as outlined at link:manual-installation.adoc[manual-installation]


.Procedure

. Note that Seldon must also be installed in your namespace, this can be done via the Open Data Hub operator link:deploying-seldon.adoc[deploying-seldon]
. Change directory to your copy of the repository.
. Copy the Open Data Hub custom resource to a place you will make edits `cp deploy/crds/opendatahub_v1alpha1_opendatahub_cr.yaml my_environment_cr.yaml`
. Open `my_environment_cr.yaml` in your editor of choice.
. Setting `odh_deploy` to `true` or `false` will either enable or disable AI Library. Edit the following lines in the file to do so.
+
....
  ai-library:
    odh_deploy: true
....
. Initiate the deployment with `oc apply -f my_environment_cr.yaml`


.Optional AI Library Configuration

At this time we do not support any optional AI Library configurations.

//.Verification steps
//(Optional) Provide the user with verification method(s) for the procedure, such as expected output or commands that can be used to check for success or failure.
To verify installation is successful:

* Check to see that all AI Library pods are running without errors.  Each model deployed should have a pod such that the name of the pod includes the name of the model.
* Verify that there is a route exposed for each model and there are no errors reported

Please refer to AI Library documentation for details on each model at link:https://gitlab.com/opendatahub/ai-library[https://gitlab.com/opendatahub/ai-library].
Note that since authentication is turned off for this deployment, you can disregard the notes there about oauth and bearer tokens.

.Additional resources

* More information about *Seldon* can be found  at link:https://www.seldon.io[www.seldon.io].

